﻿using Phobos.Library.Interfaces.Services;
using Phobos.Library.Models.ConfigSections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phobos.Library.CoreServices
{
    public class ConfigurationService : IConfigurationService
    {
        public ExternalMessagingConfigSection GetExternalMessagingConfigSection()
        {
            return (ExternalMessagingConfigSection)System.Configuration.ConfigurationManager.GetSection("externalMessagingService");
        }


        public CoreConfigSection GetCoreConfigSection()
        {
            return (CoreConfigSection)System.Configuration.ConfigurationManager.GetSection("coreConfig");
        }
    }
}
