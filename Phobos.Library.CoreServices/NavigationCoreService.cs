﻿using Phobos.Library.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phobos.Library.Models.ViewModels;
using Ninject;
using Phobos.Library.Interfaces.Repos;
using Ninject.Extensions.Logging;

namespace Phobos.Library.CoreServices
{
    public class NavigationCoreService : INavigationService
    {
        #region Injects
        [Inject]
        public IUserManagementRepo Repository { get; set; }
        [Inject]
        public ILogger logger { get; set; }
        #endregion

        public MenuEntriesListViewModel GetMenusForUser(string username)
        {
            logger.Debug(string.Format("The method {0} was requested with the following params (userName: {1})", System.Reflection.MethodBase.GetCurrentMethod().Name, username));

            var menus = new MenuEntriesListViewModel();

            menus.Title = "Main Navigation";

            menus.Add(new MenuEntriesViewModel() { Controller = "Home", Action = "Index", Text = "Home" });

            return menus;
        }

        public bool CheckIfActionIsAllowed(string currentControllerName, string currentActionName, string username)
        {
            logger.Debug(string.Format("The method {0} was requested with the following params (userName: {1}, currentControllerName: {2},  currentActionName: {3})", System.Reflection.MethodBase.GetCurrentMethod().Name, username, currentControllerName, currentActionName));

            var selectedAction = this.Repository.GetAutorizationForAction(currentControllerName, currentActionName);
            var result = false;
            var msg = "";

            if (selectedAction != null)
            {
                if (selectedAction.Roles.Any(x => x.UserAccounts.Any(z => z.Username == username)) ||
                    selectedAction.UserAccounts.Any(x => x.Username == username))
                {
                    msg = string.Format("Was found action authorizations for the specified action/controller ({0}/{1}) for user {2}.", currentActionName, currentControllerName, username);
                    
                    result = true;
                }
                else
                {
                    msg = string.Format("Was not found action authorizations for the specified action/controller ({0}/{1}) for user {2}.", currentActionName, currentControllerName, username);

                    result = false;
                }
            }
            else
            {
                //// Action is not authorized
                msg = string.Format("Was not found action authorizations for the specified action/controller ({0}/{1}).", currentActionName, currentControllerName);
                result = false;
            }

            if (!result)
            {
                logger.Warn(msg);
            }
            else
            {
                logger.Info(msg);
            }

            return result;
        }
    }
}
