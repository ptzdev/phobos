﻿using Phobos.Library.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Phobos.Library.Models;
using Phobos.Library.Interfaces.Repos;
using Ninject;
using Phobos.Library.Interfaces.Services;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using Phobos.Library.Utils;
using Phobos.Library.Models.Enums;
using Phobos.Library.Models.Messaging;
using Ninject.Extensions.Logging;
using Phobos.Library.Models.ConfigSections;

namespace Phobos.Library.CoreServices
{
    public class UserManagementCoreService : IUserManagementService
    {
        public UserManagementCoreService()
        {

        }

        #region Injects
        [Inject]
        public IUserManagementRepo Repository { get; set; }

        [Inject]
        public ILogger logger { get; set; }

        [Inject]
        public INotificationService NotificationService { get; set; }

        [Inject]
        public ICoreRepo CoreRepository { get; set; }

        [Inject]
        public IConfigurationService configSvc { get; set; }

        [Inject]
        public IExternalMessageService ExternalMessageService { get; set; }

        [Inject]
        public IExternalMessageTemplateService MessageTemplateSvc { get; set; }
        #endregion

        #region IUserManagementService

        bool CheckIfRegisterIsAllowed(string name, string userName, string password, string confirmPassword, out string msg)
        {
            msg = "";

            var userAlreadyExists = this.Repository.CheckIfUserExist(userName);

            if (userAlreadyExists)
            {
                msg = string.Format("Was requested a creation of a user for an existing username ({0}).", userName);

                logger.Info(msg);

                return false;
            }

            return true;
        }

        public bool CheckIfUserIsValid(string userName, string password, out string msg)
        {
            CoreConfigSection config = configSvc.GetCoreConfigSection();
            Configuration salt = CoreRepository.GetConfiguration("PasswordSalt");

            var selectedUser = this.Repository.GetUser(userName);
            msg = "";

            if (selectedUser != null)
            {
                //// Existing User
                if (!selectedUser.IsLocked)
                {
                    //// Check if pwd is correct
                    if (password.GetAsHash(salt.Value) == selectedUser.Password)
                    {
                        //// Update the user info to have last login.
                        if (this.Repository.UpdateLastLoginDate(userName, DateTime.Now))
                        {
                            logger.Info(string.Format("The user {0} had successfully logged-in.", userName));
                        }

                        return true;
                    }
                    else
                    {
                        //// Add Login attempt.
                        this.Repository.AddFailedLoginAttempt(userName);

                        //// Password is diferent than expected
                        msg = string.Format("Was requested a login user with the username({0}) but the passwords didn't match.", userName);

                        logger.Warn(msg);

                        return false;
                    }
                }
                else
                {
                    var unlockDate = DateTime.Now.Subtract(config.UserManagement.UnlockAccountTimeSpan);
                    if (selectedUser.LockedDate < unlockDate)
                    {
                        //// Unlock user.
                        this.Repository.UnlockUserAccount(userName);

                        //// Try again login
                        return this.CheckIfUserIsValid(userName, password, out msg);
                    }
                    else
                    {
                        //// The user account is locked
                        msg = string.Format("The user account is locked ({0}).", userName);

                        logger.Warn(msg);

                        return false;
                    }
                }
            }
            else
            {
                //// Non Existing User

                msg = string.Format("Was requested a non existing user with the username({0}).", userName);

                logger.Warn(msg);

                return false;
            }
        }

        bool CheckSecurityMesurements(string userName, string password, string confirmPassword, out string msg)
        {
            msg = "";
            bool isValid = this.CheckPassword(password, out msg);

            if (!isValid)
            {
                logger.Info(msg);
            }

            return isValid;
        }

        public List<UserTask> GetLastTasks(string userName, int qtd)
        {
            return this.Repository.GetLastTasksForUser(userName, qtd);
        }

        public UserAccount GetUser(string userName)
        {
            var selectedUser = this.Repository.GetUser(userName);
            return selectedUser;
        }

        public List<UserAccount> GetAllUsers()
        {
            var listOfUsers = this.Repository.GetAllUsers();
            return listOfUsers;
        }

        public bool RecoverProfile(string userName, out string msg)
        {
            bool result = false;
            var selectedUser = this.Repository.GetUser(userName);
            msg = "";

            if (selectedUser != null)
            {
                const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
                StringBuilder res = new StringBuilder();
                Random rnd = new Random();
                selectedUser.Password = "";

                while (!CheckPassword(selectedUser.Password, out msg))
                {
                    var length = 8;
                    while (0 < length--)
                    {
                        res.Append(valid[rnd.Next(valid.Length)]);
                    }


                    selectedUser.Password = res.ToString();
                }

                ExternalMessage recoverProfileMsg = MessageTemplateSvc.GetRecoverProfileMessage(selectedUser);

                if (ExternalMessageService.SendMessage(selectedUser, recoverProfileMsg, ref msg))
                {
                    Configuration salt = CoreRepository.GetConfiguration("PasswordSalt");
                    selectedUser.Password = selectedUser.Password.GetAsHash(salt.Value);

                    result = this.Repository.UpdateAccount(selectedUser);
                }
            }
            else
            {
                //// Non Existing User

                msg = string.Format("Was requested a non existing user with the username({0}).", userName);

                logger.Warn(msg);
            }

            return result;
        }

        public bool CreateUser(string name, string userName, string password, string confirmPassword, out string error)
        {
            Configuration salt = CoreRepository.GetConfiguration("PasswordSalt");

            UserAccount selectedUser = null;
            error = "";

            if (password == confirmPassword)
            {
                if (this.CheckIfRegisterIsAllowed(name, userName, password, confirmPassword, out error))
                {
                    if (this.CheckSecurityMesurements(userName, password, confirmPassword, out error))
                    {
                        selectedUser = this.Repository.CreateUser(name, userName, password.GetAsHash(salt.Value));
                        ExternalMessage recoverProfileMsg = MessageTemplateSvc.GetNewProfileMessage(selectedUser);

                        if (this.ExternalMessageService.SendMessage(selectedUser, recoverProfileMsg, ref error))
                        {
                            this.NotificationService.SendNotification(MessageTemplateSvc.GetWelcomeNotification(selectedUser));
                        }
                    }
                }

                return selectedUser != null;
            }
            else
            {
                error = string.Format("The confirm confirm password and the password does not match.", userName);

                logger.Warn(error);

                return false;
            }
        }

        public bool UpdateAccount(UserAccount userAccount)
        {
            return this.Repository.UpdateAccount(userAccount);
        }
        #endregion

        #region Password Verifications
        private bool CheckPassword(string password, out string msg)
        {
            Regex uppercaseCharacterMatcher = new Regex("[A-Z]");
            Regex lowerCharacterMatcher = new Regex("[a-z]");
            Regex lowerCharacterDigits = new Regex("[0-9]");
            int minimumLength = 3;
            int minimunQtdOfUpper = 1;
            int minimunQtdOfLower = 1;
            int minimunQtdOfDigits = 1;

            var haveMinimunQtdOfUpper = uppercaseCharacterMatcher.Matches(password).Count >= minimunQtdOfUpper;
            var haveMinimumLength = password.Length > minimumLength;
            var haveMinimunQtdOfLower = lowerCharacterMatcher.Matches(password).Count >= minimunQtdOfLower;
            var haveMinimunQtdOfDigits = lowerCharacterDigits.Matches(password).Count >= minimunQtdOfDigits;
            msg = "The password does not meet the security mesuraments:";
            if (!haveMinimumLength)
            {
                msg += string.Format(Environment.NewLine + " Minimum Lenth: {0} ", minimumLength);
            }

            if (!haveMinimunQtdOfUpper)
            {
                msg += string.Format(Environment.NewLine + " Minimum Qtd of Uppercases: {0} ", minimunQtdOfUpper);
            }

            if (!haveMinimunQtdOfLower)
            {
                msg += string.Format(Environment.NewLine + " Minimum Qtd of Lowercases: {0} ", minimunQtdOfLower);
            }

            if (!haveMinimunQtdOfDigits)
            {
                msg += string.Format(Environment.NewLine + " Minimum Qtd of Digits: {0} ", minimunQtdOfDigits);
            }

            if (haveMinimumLength && haveMinimunQtdOfDigits && haveMinimunQtdOfLower && haveMinimunQtdOfUpper)
            {
                msg = "";
            }
            return haveMinimumLength && haveMinimunQtdOfDigits && haveMinimunQtdOfLower && haveMinimunQtdOfUpper;
        }
        #endregion

        public void UpdateAccountForLogin(string username)
        {
            var user = this.GetUser(username);

            user.CurrentStatus = Models.Enums.UserStatusEnum.Online;

            this.NotificationService.ClearNotifications(NotificationType.Login, user.Username);

            this.NotificationService.SendNotification(MessageTemplateSvc.GetLastLoginNotification(user));

            user.LastLoginDate = DateTime.Now;

            this.UpdateAccount(user);
        }
        public void UpdateAccountForLogout(string username)
        {
            var user = this.GetUser(username);

            if (user != null)
            {
                user.CurrentStatus = Models.Enums.UserStatusEnum.Offline;

                this.UpdateAccount(user);

                logger.Info(string.Format("The user {0} had successfully logged-out.", username));
            }
        }
        public bool CreateRole(string username, out string error)
        {
            error = "";

            return this.Repository.CreateRole(username) != null;
        }
        public List<UserRole> GetAllRoles()
        {
            var listOfroles = this.Repository.GetAllRoles();
            return listOfroles;
        }
        public UserRole GetRole(string name)
        {
            var role = this.Repository.GetRole(name);
            return role;
        }
        public bool UpdateRole(string oldName, string newName, List<string> roles, out string error)
        {
            var role = this.GetRole(oldName);
            var roleIsUpdated = false;

            error = "";

            if (role != null)
            {
                var usersInRole = Repository.GetAllUsers(role).Select(x => x.Username).ToList();

                role.Name = newName;

                roleIsUpdated = this.Repository.UpdateRole(role, oldName);

                if (usersInRole.Count > 0)
                {
                    return this.Repository.UpdateRoleUsers(role.Name, usersInRole);
                }

                return roleIsUpdated;
            }
            else
            {
                error = "Non Existing Role";
            }

            return roleIsUpdated;
        }
        public bool DeleteRole(string name, out string error)
        {
            error = "";
            return this.Repository.DeleteRole(name);
        }
        public bool AddUserToRole(string username, string roleName)
        {
            return this.Repository.AddUserToRole(username, roleName);
        }
        public bool DeleteUser(string username)
        {
            return this.Repository.DeleteUser(username);
        }
        public bool ChangeUserPassword(string username, string oldpassword, string password, out string error)
        {
            var userAccount = this.GetUser(username);
            error = "";

            Configuration salt = CoreRepository.GetConfiguration("PasswordSalt");
            if (userAccount.Password == oldpassword.GetAsHash(salt.Value))
            {
                userAccount.Password = password.GetAsHash(salt.Value);
                return this.UpdateAccount(userAccount);
            }
            else
            {
                error = "The old passwrod is incorrect";
            }

            return false;
        }
    }
}
