﻿using Ninject;
using Ninject.Extensions.Logging;
using Phobos.Library.Interfaces.Repos;
using Phobos.Library.Interfaces.Services;
using Phobos.Library.Models;
using Phobos.Library.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phobos.Library.CoreServices
{
    public class NotificationService : INotificationService
    {
        #region Injects
        [Inject]
        public INotificationRepo Repository { get; set; }
        [Inject]
        public ILogger logger { get; set; }
        #endregion

        public bool SendNotification(UserNotification userNotification)
        {
            logger.Debug(string.Format("The method {0} was requested with the following params (userName: {1})", System.Reflection.MethodBase.GetCurrentMethod().Name, userNotification.User.Username));

            return this.Repository.SendNotification(userNotification);
        }

        public List<UserNotification> GetLastNotifications(string userName, int qtd, bool onlyUnread)
        {
            logger.Debug(string.Format("The method {0} was requested with the following params (userName: {1},qtd: {2},onlyUnread: {3})", System.Reflection.MethodBase.GetCurrentMethod().Name, userName, qtd, onlyUnread));

            return this.Repository.GetLastNotificationsForUser(userName, qtd, true);
        }

        public void ClearNotifications(NotificationType notificationType, string userName)
        {
            logger.Debug(string.Format("The method {0} was requested with the following params (userName: {1})", System.Reflection.MethodBase.GetCurrentMethod().Name, userName));

            this.Repository.ClearNotifications(notificationType, userName);
        }


        public void MarkNotificationAsRead(int id)
        {
            logger.Debug(string.Format("The method {0} was requested with the following params (id: {1})", System.Reflection.MethodBase.GetCurrentMethod().Name, id));

            this.Repository.MarkNotificationAsRead(id);
        }

        public List<UserNotification> GetNotifications(string userName)
        {
            logger.Debug(string.Format("The method {0} was requested with the following params (userName: {1})", System.Reflection.MethodBase.GetCurrentMethod().Name, userName));

            return this.Repository.GetNotifications(userName);
        }
        
        public void DeleteNotification(string userName, int selectedInt)
        {
            logger.Debug(string.Format("The method {0} was requested with the following params (userName: {1}, selectedInt: {2})", System.Reflection.MethodBase.GetCurrentMethod().Name, userName, selectedInt));

            this.Repository.DeleteNotification(userName, selectedInt);
        }
    }
}
