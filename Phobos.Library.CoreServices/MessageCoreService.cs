﻿using Ninject;
using Ninject.Extensions.Logging;
using Phobos.Library.Interfaces;
using Phobos.Library.Interfaces.Repos;
using Phobos.Library.Interfaces.Services;
using Phobos.Library.Models;
using Phobos.Library.Models.Enums;
using Phobos.Library.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phobos.Library.CoreServices
{
    public class MessageCoreService : IMessageService
    {
        [Inject]
        public IMessageRepo Repository { get; set; }

        [Inject]
        public ILogger logger { get; set; }

        public List<UserMessage> GetLastMessages(string userName, int qtd)
        {
            logger.Debug(string.Format("The method {0} was requested with the following params (userName: {1}, qtd: {2})", System.Reflection.MethodBase.GetCurrentMethod().Name, userName, qtd));

            return this.Repository.GetLastMessages(userName, qtd);
        }

        public List<UserMessageFolder> GetAllFoldersForUser(string userName)
        {
            logger.Debug(string.Format("The method {0} was requested with the following params (userName: {1})", System.Reflection.MethodBase.GetCurrentMethod().Name, userName));

            return this.Repository.GetAllFolders(userName);
        }
        public UserMessageFolder GetFolder(string userName, int? id)
        {
            logger.Debug(string.Format("The method {0} was requested with the following params (userName: {1}, id?: {2})", System.Reflection.MethodBase.GetCurrentMethod().Name, userName, id));
            UserMessageFolder folder = null;

            if (id.HasValue)
            {
                logger.Debug(string.Format("Folder {0} was requested", id));

                folder = this.Repository.GetFolder(userName, id.Value);

                logger.Debug(string.Format("Folder {0} was found and will retreive the messages", id));

                folder.Messages = this.Repository.GetMessages(userName, folder.Id);

                logger.Debug(string.Format("Messages for folder {0} were retreived successfully", id));
            }
            else
            {
                logger.Debug(string.Format("Inbox folder was requested", id));

                folder = this.Repository.GetInboxFolder(userName);

                folder.Messages = this.Repository.GetMessages(userName, folder.Id);
            }

            if (folder == null)
            {
                logger.Debug(string.Format("Folder was not found {0}", id));

                folder = this.Repository.GetInboxFolder(userName);
            }

            return folder;
        }
        public UserMessage SendMessage(string userName, UserMessage createdMessage)
        {
            logger.Debug(string.Format("The method {0} was requested with the following params (userName: {1})", System.Reflection.MethodBase.GetCurrentMethod().Name, userName));

            UserMessageFolder sentFolder = this.Repository.GetSentFolder(userName);

            if (createdMessage.Sender.Username == userName)
            {
                //// Create a new instance of this message and send it;
                UserMessage sentMessage = new UserMessage()
                {
                    Attachments = createdMessage.Attachments,
                    Folder = createdMessage.Folder,
                    HasAttachment = createdMessage.HasAttachment,
                    IsFavorite = createdMessage.IsFavorite,
                    Message = createdMessage.Message,
                    Title = createdMessage.Title,
                    MessageDate = DateTime.Now,
                    SendDate = DateTime.Now,
                    Owner = new UserAccount() { Username = createdMessage.Receiver.Username },
                    Receiver = new UserAccount() { Username = createdMessage.Receiver.Username },
                    Sender = new UserAccount() { Username = createdMessage.Owner.Username },
                    Sent = true,
                    IsDraft = false
                };

                this.Repository.SaveMessage(sentMessage);

                //// Mark the current message as Sent.
                createdMessage.Sent = true;
                createdMessage.SendDate = DateTime.Now;
                createdMessage.Folder = sentFolder;
                this.Repository.SaveMessage(createdMessage);
            }

            return createdMessage;
        }
        public UserMessage SaveMessage(string userName, UserMessage newMessage)
        {
            logger.Debug(string.Format("The method {0} was requested with the following params (userName: {1})", System.Reflection.MethodBase.GetCurrentMethod().Name, userName));

            if (newMessage.Owner.Username == userName)
            {
                return this.Repository.SaveMessage(newMessage);
            }

            return null;
        }
        public void DeleteMessage(string userName, int messageId)
        {
            logger.Debug(string.Format("The method {0} was requested with the following params (userName: {1}, messageId: {2})", System.Reflection.MethodBase.GetCurrentMethod().Name, userName, messageId));

            this.Repository.DeleteMessage(messageId);
        }
        public UserMessage GetMessage(string userName, int id)
        {
            logger.Debug(string.Format("The method {0} was requested with the following params (userName: {1}, id: {2})", System.Reflection.MethodBase.GetCurrentMethod().Name, userName, id));

            return this.Repository.GetMessage(userName, id);
        }
        public UserMessageFolder SaveFolder(string userName, UserMessageFolder model)
        {
            logger.Debug(string.Format("The method {0} was requested with the following params (userName: {1})", System.Reflection.MethodBase.GetCurrentMethod().Name, userName));

            return this.Repository.SaveFolder(model);
        }
        public void MoveMessageToFolder(string userName, int msgId, int newFolderId)
        {
            logger.Debug(string.Format("The method {0} was requested with the following params (userName: {1}, msgId: {2}, newFolderId:{3})", System.Reflection.MethodBase.GetCurrentMethod().Name, userName, msgId, newFolderId));

            this.Repository.MoveMessageToFolder(userName, msgId, newFolderId);
        }
        public void DeleteFolder(string userName, int id)
        {
            logger.Debug(string.Format("The method {0} was requested with the following params (userName: {1}, id: {2})", System.Reflection.MethodBase.GetCurrentMethod().Name, userName, id));

            this.Repository.DeleteFolder(userName, id);
        }
    }
}
