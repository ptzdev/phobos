﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phobos.Library.Models.ViewModels
{
    public class UserAccountDetailViewModel
    {
        public UserAccountViewModel ProfileDetails { get; set; }

        public ChangePasswordViewModel ChangePassword { get; set; }
    }
}
