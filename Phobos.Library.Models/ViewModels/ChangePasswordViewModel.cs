﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phobos.Library.Models.ViewModels
{
    public class ChangePasswordViewModel
    {
        [Required]
        public string Password { get; set; }
        [Required]
        [Display(Name="Confirm Password")]
        public string ConfirmPassword { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        [Display(Name = "Old Password")]
        public string OldPassword { get; set; }
    }
}
