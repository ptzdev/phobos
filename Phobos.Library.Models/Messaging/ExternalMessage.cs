﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phobos.Library.Models.Messaging
{
    public class ExternalMessage
    {

        public ExternalMessage()
        {
            this.To = new List<string>();
            this.Cc = new List<string>();
            this.Bcc = new List<string>();
        }

        public ExternalMessage(string toEmail):this()
        {
            this.To.Add(toEmail);
        }

        public List<string> To { get; set; }
        public List<string> Cc { get; set; }
        public List<string> Bcc { get; set; }
        public string Subject { get; set; }

        public string Body { get; set; }
    }
}
