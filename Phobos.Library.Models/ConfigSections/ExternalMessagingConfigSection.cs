﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phobos.Library.Models.ConfigSections
{
    public class ExternalMessagingConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("BaseUrl", DefaultValue = "https://api.mailgun.net/v3/sandbox11f65e9f553d42e3ba1d57a4266bb962.mailgun.org", IsRequired = true)]
        public string BaseUrl
        {
            get
            {
                return (string)this["BaseUrl"];
            }
            set
            {
                this["BaseUrl"] = value;
            }
        }

        [ConfigurationProperty("ApiKey", DefaultValue = "key-ed6268300ba23c819a3482e348672f3f", IsRequired = true)]
        public string ApiKey
        {
            get
            {
                return (string)this["ApiKey"];
            }
            set
            {
                this["ApiKey"] = value;
            }
        }


        [ConfigurationProperty("TestMode", DefaultValue = false, IsRequired = false)]
        public bool TestMode
        {
            get
            {
                return (bool)this["TestMode"];
            }
            set
            {
                this["TestMode"] = value;
            }
        }

        [ConfigurationProperty("Domain")]
        public DomainElement Domain
        {
            get
            {
                return (DomainElement)this["Domain"];
            }
            set
            { this["Domain"] = value; }
        }

        [ConfigurationProperty("From")]
        public FromElement From
        {
            get
            {
                return (FromElement)this["From"];
            }
            set
            { this["From"] = value; }
        }


    }
}
