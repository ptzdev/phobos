﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Phobos.Library.Models.ConfigSections
{
    public class UserManagementElement : ConfigurationElement
    {
        [ConfigurationProperty("Minutes", DefaultValue = "30", IsRequired = false)]
        public int Minutes
        {
            get
            {
                return Convert.ToInt32(this["Minutes"]);
            }
            set
            {
                this["Minutes"] = value;
            }
        }
        [ConfigurationProperty("Seconds", DefaultValue = "0", IsRequired = false)]
        public int Seconds
        {
            get
            {
                return Convert.ToInt32(this["Seconds"]);
            }
            set
            {
                this["Seconds"] = value;
            }
        }
        public TimeSpan UnlockAccountTimeSpan
        {
            get
            {
                return new TimeSpan(0, this.Minutes, this.Seconds);
            }
        }
    }
}
