﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Phobos.Library.Models.ConfigSections
{
    public class DomainElement : ConfigurationElement
    {
        [ConfigurationProperty("Name", DefaultValue = "sandbox11f65e9f553d42e3ba1d57a4266bb962.mailgun.org", IsRequired = true)]
        public String Name
        {
            get
            {
                return (String)this["Name"];
            }
            set
            {
                this["Name"] = value;
            }
        }
    }
}
