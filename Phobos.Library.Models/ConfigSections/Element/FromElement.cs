﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Phobos.Library.Models.ConfigSections
{
    public class FromElement : ConfigurationElement
    {
        [ConfigurationProperty("Email", DefaultValue = "noreply@phobos.pt", IsRequired = true)]
        public string Email
        {
            get
            {
                return (string)this["Email"];
            }
            set
            {
                this["Email"] = value;
            }
        }

        [ConfigurationProperty("Name", DefaultValue = "Phobos", IsRequired = true)]
        public string Name
        {
            get
            {
                return (string)this["Name"];
            }
            set
            {
                this["Name"] = value;
            }
        }
    }
}
