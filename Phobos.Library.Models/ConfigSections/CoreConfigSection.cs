﻿using System.Configuration;

namespace Phobos.Library.Models.ConfigSections
{
    public class CoreConfigSection : ConfigurationSection
    {

        [ConfigurationProperty("UserManagement")]
        public UserManagementElement UserManagement
        {
            get
            {
                return (UserManagementElement)this["UserManagement"];
            }
            set
            {
                this["UserManagement"] = value;
            }
        }
    }
}
