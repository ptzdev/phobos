﻿using Phobos.Library.Models.ConfigSections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Phobos.Library.ExternalMessaging
{
    public class ExternalMessagingException : Exception
    {
        private HttpStatusCode reponseStatusCode;

        public ExternalMessagingException(string msg, ExternalMessagingConfigSection config, HttpStatusCode httpStatusCode) :
            base(msg)
        {
            this.reponseStatusCode = httpStatusCode;
            base.Data.Add("ApiKey", config.ApiKey);
            base.Data.Add("BaseUrl", config.BaseUrl);
            base.Data.Add("DomainName", config.Domain.Name);
        }
    }
}
