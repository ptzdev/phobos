﻿using Ninject;
using Ninject.Extensions.Logging;
using Phobos.Library.Interfaces.Services;
using Phobos.Library.Models;
using Phobos.Library.Models.ConfigSections;
using Phobos.Library.Models.Messaging;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phobos.Library.ExternalMessaging
{
    public class MailGunExternalMessaging : IExternalMessageService
    {
        [Inject]
        public ILogger logger { get; set; }

        [Inject]
        public IConfigurationService configSvc { get; set; }

        public bool SendMessage(UserAccount selectedUser, ExternalMessage externalMsg, ref string msg)
        {
            ExternalMessagingConfigSection config = configSvc.GetExternalMessagingConfigSection();

            logger.Debug(string.Format("The method {0} was requested with the following params (userName: {1})", System.Reflection.MethodBase.GetCurrentMethod().Name, selectedUser.Username));

            msg = "";

            if (!string.IsNullOrEmpty(externalMsg.Subject) || !string.IsNullOrEmpty(externalMsg.Body))
            {
                RestClient client = new RestClient();
                client.BaseUrl = new Uri(config.BaseUrl);
                client.Authenticator = new HttpBasicAuthenticator("api", config.ApiKey);
                RestRequest request = new RestRequest(Method.POST);
                request.AddParameter("domain", config.Domain.Name, ParameterType.UrlSegment);
                request.Resource = "{domain}/messages";
                request.AddParameter("from", string.Format("{0} <{1}>", config.From.Name, config.From.Email));
                request.AddParameter("subject", externalMsg.Subject);
                request.AddParameter("text", externalMsg.Body);

                foreach (string to in externalMsg.To)
                {
                    request.AddParameter("to", to);
                }

                foreach (string cc in externalMsg.Cc)
                {
                    request.AddParameter("cc", cc);
                }

                foreach (string bcc in externalMsg.Bcc)
                {
                    request.AddParameter("bcc", bcc);
                }

                if (config.TestMode)
                {
                    request.AddParameter("o:testmode", "yes");
                }

                var response = client.Execute(request);

                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    msg = string.Format("An error occur when the system was sending the email {0} {1} {2}", response.StatusDescription, msg, response.Content);

                    if (config.TestMode && 
                        response.Content.Contains("Sandbox subdomains are for test purposes only. Please add your own domain or add the address to authorized recipients in domain settings."))
                    {
                        msg = string.Format("Message wasn't sent because {0} ", response.Content);
                        response.StatusCode = System.Net.HttpStatusCode.OK;
                    }
                    else
                    {
                        logger.Error(msg);

                        ExternalMessagingException externalMessagingException = new ExternalMessagingException(msg, config, response.StatusCode);

                        throw externalMessagingException;
                    }
                }
                else
                {
                    msg = "Message sent with sucess";
                }

                return response.StatusCode == System.Net.HttpStatusCode.OK;
            }
            else
            {
                msg = "The Subject and the Body was empty";

                return false;
            }
        }
    }
}
