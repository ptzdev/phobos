﻿using Phobos.Library.Interfaces.Services;
using Phobos.Library.Models;
using Phobos.Library.Models.Enums;
using Phobos.Library.Models.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phobos.Library.ExternalMessaging
{
    public class ExternalMessageTemplateService : IExternalMessageTemplateService
    {
        public ExternalMessage GetRecoverProfileMessage(UserAccount selectedUser)
        {
            return new ExternalMessage(selectedUser.Username)
            {
                Subject = "Your password has been reset",
                Body = string.Format("Hi {0}\n\rYour new password is {1}", selectedUser.FirstName, selectedUser.Password)
            };
        }

        public ExternalMessage GetNewProfileMessage(UserAccount selectedUser)
        {
            return new ExternalMessage(selectedUser.Username)
            {
                Subject = "A new account has been created",
                Body = string.Format("Hi {0}\n\rA new account has been created for your user {1}\n\rYour new password is {2}", selectedUser.FirstName, selectedUser.Username, selectedUser.Password)
            };
        }


        public UserNotification GetWelcomeNotification(UserAccount selectedUser)
        {
            return new UserNotification()
            {
                Type = NotificationType.Welcome,
                Read = false,
                Icon = "child",
                IconColor = TextColor.Blue,
                Title = "Welcome!!!",
                User = selectedUser
            };
        }


        public UserNotification GetLastLoginNotification(UserAccount user)
        {
            return new UserNotification()
            {
                Type = NotificationType.Login,
                Read = false,
                User = user,
                Icon = "search-minus",
                IconColor = TextColor.Blue,
                Title = string.Format("Welcome back {0} last time you been here was {1}.", user.FirstName, user.LastLoginDate.ToString()),
            };
        }
    }
}
