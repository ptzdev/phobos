﻿using Phobos.Library.Models.ConfigSections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phobos.Library.Interfaces.Services
{
    public interface IConfigurationService
    {
        ExternalMessagingConfigSection GetExternalMessagingConfigSection();

        CoreConfigSection GetCoreConfigSection();
    }
}
