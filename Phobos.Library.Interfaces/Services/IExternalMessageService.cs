﻿using Phobos.Library.Models;
using Phobos.Library.Models.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phobos.Library.Interfaces.Services
{
    public interface IExternalMessageService
    {
        bool SendMessage(UserAccount selectedUser, ExternalMessage recoverProfileMsg, ref string msg);
    }
}
