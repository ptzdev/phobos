﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Phobos.Library.Utils;

namespace Phobos.UnitTest.Phobos.Library.Utils
{
    [TestClass]
    public class Extensions_UnitTest
    {
        [TestMethod]
        public void GetAsHash()
        {
            var salt = "Phobos";
            var expectedHash = "bc3f391bbfc38df720519faa789a2950";
            var password = "Password#123";
            Assert.AreEqual(expectedHash, password.GetAsHash(salt));
        }

        [TestMethod]
        public void TruncateLongString()
        {
            int positions = 80;
            var longString = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut tortor a turpis volutpat feugiat. Vivamus malesuada sed tellus ac dictum. Proin luctus consectetur posuere. Aenean mauris elit, malesuada nec consequat vel, dignissim sed arcu. Etiam ut leo massa. Aliquam a tellus rhoncus, rhoncus justo eu, consequat ligula. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut ut interdum sem, non sodales lectus.";
            var expectedString = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut tortor a tur...";
            var truncatedString = longString.TruncateLongString(positions, "...", true);
            Assert.AreEqual(expectedString, truncatedString);
            Assert.AreEqual(longString.Substring(0, positions), truncatedString.Substring(0, positions));
        }

        [TestMethod]
        public void TruncateLongString_shortString()
        {
            int positions = 80;
            var longString = "Lorem ipsum dolor sit amet";
            var expectedString = "Lorem ipsum dolor sit amet";
            var truncatedString = longString.TruncateLongString(positions, "...", false);
            Assert.AreEqual(expectedString, truncatedString);
        }


        [TestMethod]
        public void TruncateLongString_NullString()
        {
            int positions = 80;
            string longString = null;
            string expectedString = "";
            var truncatedString = longString.TruncateLongString(positions, "...", false);
            Assert.AreEqual(expectedString, truncatedString);
        }
    }
}
