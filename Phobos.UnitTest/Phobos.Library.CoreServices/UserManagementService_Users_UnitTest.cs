﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Phobos.Library.CoreServices;
using Phobos.Library.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using System.IO;
using Phobos.Library.Interfaces.Repos;
using System.Threading;

namespace Phobos.UnitTest.Phobos.Library.CoreServices
{
    [TestClass]
    public class UserManagementService_Users_UnitTest
    {
        public IUserManagementService userMngSvc { get; set; }
        public ICoreRepo coreRepo { get; set; }

        [TestInitialize]
        public void Initialize()
        {
            var error = "";
            var kernel = UnitTestUtils.GetMockedKernel();
            kernel.Rebind<IUserManagementService>().To<UserManagementCoreService>();
            userMngSvc = kernel.Get<IUserManagementService>();

            coreRepo = kernel.Get<ICoreRepo>();
            coreRepo.AddConfiguration("PasswordSalt", "Phobos");

            this.CreateUser();
        }

        [TestCleanup]
        public void CleanUp()
        {
            foreach (var user in userMngSvc.GetAllUsers())
            {
                userMngSvc.DeleteUser(user.Username);
            }
        }

        [TestMethod]
        public void ChangeUserPassword()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 8);
            var username = name + "@phobos.pt";
            var pwd = "Qwerty#123";
            var error = "";
            var result = userMngSvc.CreateUser(name, username, pwd, pwd, out error);

            result = userMngSvc.ChangeUserPassword(username, pwd, pwd + "4", out error);
            Assert.IsTrue(result, string.Format("Couldn't change the password for {0} because {1}", username, error));
        }

        [TestMethod]
        public void ChangeUserPassword_DiferentPasswoed()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 8);
            var username = name + "@phobos.pt";
            var pwd = "Qwerty#123";
            var error = "";
            var result = userMngSvc.CreateUser(name, username, pwd, pwd, out error);

            result = userMngSvc.ChangeUserPassword(username, pwd + "5", pwd + "4", out error);
            Assert.IsFalse(result, string.Format("Couldn't change the password for {0} because {1}", username, error));
        }

        [TestMethod]
        public void CreateUser()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 8);
            var username = name + "@phobos.pt";
            var pwd = "Qwerty#123";
            var error = "";
            var result = userMngSvc.CreateUser(name, username, pwd, pwd, out error);
            Assert.IsTrue(result, string.Format("Couldn't create the user {0}", username));
        }

        [TestMethod]
        public void CreateUserExistingUser()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 8);
            var username = name + "@phobos.pt";
            var pwd = "Qwerty#123";
            var error = "";
            var result = userMngSvc.CreateUser(name, username, pwd, pwd, out error);
            Assert.IsTrue(result, string.Format("Couldn't create the user {0}", username));
            result = userMngSvc.CreateUser(name, username, pwd, pwd, out error);
            Assert.IsFalse(result, string.Format("Couldn't create the user {0}", username));
        }

        [TestMethod]
        public void CreateUserWithoutPassword()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 8);
            var username = name + "@phobos.pt";
            var pwd = "";
            var error = "";
            var result = userMngSvc.CreateUser(name, username, pwd, pwd, out error);

            Assert.IsFalse(result, error);
            Assert.IsFalse(string.IsNullOrEmpty(error), error);
        }

        [TestMethod]
        public void CreateUserWithDiferentPasswords()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 8);
            var username = name + "@phobos.pt";
            var error = "";
            var result = userMngSvc.CreateUser(name, username, null, string.Empty, out error);

            Assert.IsFalse(result, error);
            Assert.IsFalse(string.IsNullOrEmpty(error), error);
        }

        [TestMethod]
        public void GetAllUsers()
        {
            var result = userMngSvc.GetAllUsers();

            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public void CheckIfUserIsValid()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 8);
            var username = name + "@phobos.pt";
            var pwd = "Qwerty#123";
            var error = "";
            var result = userMngSvc.CreateUser(name, username, pwd, pwd, out error);
            Assert.IsTrue(result, string.Format("Couldn't create the user {0}", username));
            result = userMngSvc.CheckIfUserIsValid(username, pwd, out error);
            Assert.IsTrue(result, string.Format("Couldn't validate the password for user {0}", username));
        }

        [TestMethod]
        public void CheckIfUserIsValid_NonExistingUser()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 8);
            var username = name + "@phobos.pt";
            var pwd = "Qwerty#123";
            var error = "";
            var result = userMngSvc.CheckIfUserIsValid(username, pwd, out error);
            Assert.IsFalse(result, string.Format("Couldn't validate the password for user {0}", username));
        }

        [TestMethod]
        public void CheckIfUserIsValid_CorrectPassword()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 8);
            var username = name + "@phobos.pt";
            var pwd = "Qwerty#123";
            var error = "";
            var result = userMngSvc.CreateUser(name, username, pwd, pwd, out error);
            Assert.IsTrue(result, string.Format("Couldn't create the user {0}", username));
            result = userMngSvc.CheckIfUserIsValid(username, pwd, out error);
            Assert.IsTrue(result, string.Format("Couldn't validate the password for user {0}", username));
        }

        [TestMethod]
        public void CheckIfUserIsValid_IncorrectPassword()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 8);
            var username = name + "@phobos.pt";
            var pwd = "Qwerty#123";
            var error = "";
            var result = userMngSvc.CreateUser(name, username, pwd, pwd, out error);
            Assert.IsTrue(result, string.Format("Couldn't create the user {0}", username));
            result = userMngSvc.CheckIfUserIsValid(username, pwd + "_", out error);
            Assert.IsFalse(result, string.Format("Couldn't validate the password for user {0}", username));
        }

        [TestMethod]
        public void CheckIfUserIsValid_Locked()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 8);
            var username = name + "@phobos.pt";
            var pwd = "Qwerty#123";
            var error = "";
            var result = userMngSvc.CreateUser(name, username, pwd, pwd, out error);
            Assert.IsTrue(result, string.Format("Couldn't create the user {0}", username));
            result = userMngSvc.CheckIfUserIsValid(username, pwd + "_", out error);
            Assert.IsFalse(result, string.Format("Couldn't validate the password for user {0}", username));
        }

        [TestMethod]
        public void CheckIfUserIsValid_LockUser()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 8);
            var username = name + "@phobos.pt";
            var pwd = "Qwerty#123";
            var error = "";
            var result = userMngSvc.CreateUser(name, username, pwd, pwd, out error);
            Assert.IsTrue(result, string.Format("Couldn't create the user {0}", username));
            for (int i = 0; i < 5; i++)
            {
                result = userMngSvc.CheckIfUserIsValid(username, pwd + "_", out error);
                Assert.IsFalse(result, string.Format("Couldn't validate the password for user {0}", username));
            }
        }

        [TestMethod]
        public void CheckIfUserIsValid_UnlockUserAfterTimne()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 8);
            var username = name + "@phobos.pt";
            var pwd = "Qwerty#123";
            var error = "";
            var result = userMngSvc.CreateUser(name, username, pwd, pwd, out error);
            Assert.IsTrue(result, string.Format("Couldn't create the user {0}", username));
            for (int i = 0; i < 6; i++)
            {
                result = userMngSvc.CheckIfUserIsValid(username, pwd + "_", out error);
                Assert.IsFalse(result, string.Format("Couldn't validate the password for user {0}", username));

                if (i >= 4)
                {
                    Thread.Sleep(500);
                }
            }

            result = userMngSvc.CheckIfUserIsValid(username, pwd, out error);
            Assert.IsTrue(result, string.Format("Couldn't validate the password for user {0}", username));
        }

        [TestMethod]
        public void UpdateAccountForLogin()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 8);
            var username = name + "@phobos.pt";
            var pwd = "Qwerty#123";
            var error = "";
            var result = userMngSvc.CreateUser(name, username, pwd, pwd, out error);
            userMngSvc.UpdateAccountForLogin(username);
        }

        [TestMethod]
        public void UpdateAccountForLogout()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 8);
            var username = name + "@phobos.pt";
            var pwd = "Qwerty#123";
            var error = "";
            var result = userMngSvc.CreateUser(name, username, pwd, pwd, out error);
            userMngSvc.UpdateAccountForLogout(username);
        }

        [TestMethod]
        public void GetLastTasks()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 8);
            var username = name + "@phobos.pt";
            var pwd = "Qwerty#123";
            var error = "";
            var result = userMngSvc.CreateUser(name, username, pwd, pwd, out error);

            var tasks = userMngSvc.GetLastTasks(username, 10);

            Assert.IsNotNull(tasks);
        }

        [TestMethod]
        public void RecoverProfile_NonExistingUser()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 8);
            var username = name + "@phobos.pt";
            var error = "";
            var result = userMngSvc.RecoverProfile(username, out error);
            Assert.IsFalse(result, string.Format("Couldn't create the user {0}", username));
        }

        [TestMethod]
        public void RecoverProfile()
        {
            var name = Guid.NewGuid().ToString().Substring(0, 8);
            var username = name + "@phobos.pt";
            var pwd = "Qwerty#123";
            var error = "";
            var result = userMngSvc.CreateUser(name, username, pwd, pwd, out error);
            Assert.IsTrue(result, string.Format("Couldn't create the user {0}", username));

            result = userMngSvc.RecoverProfile(username, out error);
            Assert.IsTrue(result, string.Format("Couldn't create the user {0}", username));
        }
    }
}
