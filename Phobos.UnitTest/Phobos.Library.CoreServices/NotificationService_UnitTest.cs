﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Phobos.Library.CoreServices;
using Phobos.Library.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using Phobos.Library.Models;
using Phobos.UnitTest;
using System.IO;

namespace Phobos.UnitTest.Phobos.Library.CoreServices
{
    [TestClass]
    public class NotificationService_UnitTest
    {
        [ClassInitialize]
        public static void SetUp(TestContext context)
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", Path.Combine(context.TestDeploymentDir, string.Empty));
        }

        public TestContext TestContext { get; set; }

        public INotificationService notificationService { get; set; }
        public IExternalMessageTemplateService externalMessageTemplateService { get; set; }

        [TestInitialize]
        public void Initialize()
        {
            var kernel = UnitTestUtils.GetMockedKernel();
            kernel.Rebind<INotificationService>().To<NotificationService>();

            externalMessageTemplateService = kernel.Get<IExternalMessageTemplateService>();
            notificationService = kernel.Get<INotificationService>();
            
        }

        [TestMethod]
        [TestProperty("UserAccount_Username", "dummyUser@phobos.pt")]
        public void SendWelcomeNotification()
        {
            //var currenMethod = GetType().GetMethod(System.Reflection.MethodInfo.GetCurrentMethod().Name);
            //UserAccount userAccount = UnitTestUtils.GetUserAccountBasedOnTestProperties(currenMethod);
            //UserNotification userNotification = externalMessageTemplateService.GetWelcomeNotification(userAccount);

            //var result = notificationService.SendNotification(userNotification);

            //Assert.IsTrue(result);
        }
    }
}
