﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Phobos.Library.CoreServices;
using Phobos.Library.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using System.IO;
using Phobos.Library.Interfaces.Repos;

namespace Phobos.UnitTest.Phobos.Library.CoreServices
{
    [TestClass]
    public class UserManagementService_Roles_UnitTest
    {
        private string roleNameToUpdate;
        private string roleNameToDelete;
        private string roleNameWithoutUserToUpdate;
        private string username;

        public List<string> createdRoles { get; set; }
        public IUserManagementService userMngSvc { get; set; }
        public ICoreRepo coreRepo { get; set; }

        [TestInitialize]
        public void Initialize()
        {
            var error = "";
            var kernel = UnitTestUtils.GetMockedKernel();
            kernel.Rebind<IUserManagementService>().To<UserManagementCoreService>();
            userMngSvc = kernel.Get<IUserManagementService>();

            coreRepo = kernel.Get<ICoreRepo>();
            coreRepo.AddConfiguration("PasswordSalt", "Phobos");

            createdRoles = new List<string>();

            roleNameToDelete = Guid.NewGuid().ToString().Substring(0, 8);
            createdRoles.Add(roleNameToDelete);
            userMngSvc.CreateRole(roleNameToDelete, out error);

            roleNameToUpdate = Guid.NewGuid().ToString().Substring(0, 8);
            createdRoles.Add(roleNameToUpdate);
            userMngSvc.CreateRole(roleNameToUpdate, out error);

            var name = Guid.NewGuid().ToString().Substring(0, 8);
            username = name + "@phobos.pt";
            var pwd = "Qwerty#123";

            userMngSvc.CreateUser(name, username, pwd, pwd, out  error);
            userMngSvc.AddUserToRole(username, roleNameToUpdate);

            roleNameWithoutUserToUpdate = Guid.NewGuid().ToString().Substring(0, 8);
            createdRoles.Add(roleNameWithoutUserToUpdate);
            userMngSvc.CreateRole(roleNameWithoutUserToUpdate, out error);
        }

        [TestCleanup]
        public void CleanUp()
        {
            string error = "";
            foreach (var createdRole in createdRoles)
            {
                userMngSvc.DeleteRole(createdRole, out error);
            }

            userMngSvc.DeleteUser(username);
        }

        [TestMethod]
        public void CreateRoles()
        {
            var error = "";
            var roleName = Guid.NewGuid().ToString().Substring(0, 8);
            createdRoles.Add(roleName);
            var result = userMngSvc.CreateRole(roleName, out error);

            Assert.IsTrue(string.IsNullOrEmpty(error), error);
            Assert.IsTrue(result, string.Format("Couldn't create the Role {0}", roleName));
        }

        [TestMethod]
        public void DeleteRole()
        {
            var error = "";
            var result = userMngSvc.DeleteRole(roleNameToDelete, out error);

            Assert.IsTrue(string.IsNullOrEmpty(error), error);
            Assert.IsTrue(result, string.Format("Couldn't create the Role {0}", roleNameToDelete));
        }

        [TestMethod]
        public void UpdateRole()
        {
            var error = "";
            var result = userMngSvc.UpdateRole(roleNameToUpdate, roleNameToUpdate + "_1", null, out error);
            roleNameToUpdate = roleNameToUpdate + "_1";
            
            createdRoles.Add(roleNameToUpdate);

            Assert.IsTrue(string.IsNullOrEmpty(error), error);
            Assert.IsTrue(result, string.Format("Couldn't create the Role {0}", roleNameToUpdate));
        }

        [TestMethod]
        public void UpdateRoleWithoutUsers()
        {
            var error = "";
            var result = userMngSvc.UpdateRole(roleNameWithoutUserToUpdate, roleNameWithoutUserToUpdate + "_1", null, out error);
            roleNameToUpdate = roleNameWithoutUserToUpdate + "_1";

            createdRoles.Add(roleNameWithoutUserToUpdate);

            Assert.IsTrue(string.IsNullOrEmpty(error), error);
            Assert.IsTrue(result, string.Format("Couldn't create the Role {0}", roleNameWithoutUserToUpdate));
        }

        [TestMethod]
        public void UpdateInexistingRole()
        {
            var error = "";
            var roleName = Guid.NewGuid().ToString().Substring(0, 8);
            var result = userMngSvc.UpdateRole(roleName, roleName + "_1", null, out error);

            Assert.IsFalse(result, string.Format("Role {0} was updated", roleName));
        }

        [TestMethod]
        public void GetAllRoles()
        {
            var roles = userMngSvc.GetAllRoles();

            Assert.IsTrue(roles.Count > 0, "Wasn't possible get roles");
        }

    }
}
