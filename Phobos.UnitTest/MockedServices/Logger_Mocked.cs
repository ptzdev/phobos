﻿using log4net;
using Ninject.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phobos.UnitTest.MockedServices
{
    public class Logger_Mocked : ILogger
    {
        private ILog logger;
        public Logger_Mocked()
        {
            log4net.Config.XmlConfigurator.Configure();
            logger = LogManager.GetLogger(typeof(UnitTestUtils));
        }

        public void Debug(Exception exception, string format, params object[] args)
        {
            logger.Debug(exception.Message, exception);
        }
        public void Debug(string format, params object[] args)
        {
            logger.DebugFormat(format,args);
        }
        public void Debug(string message)
        {
            logger.Debug(message);
        }
        public void DebugException(string message, Exception exception)
        {
            logger.Debug(message, exception);
        }
        public void Error(Exception exception, string format, params object[] args)
        {
            logger.Error(exception.Message, exception);
        }
        public void Error(string format, params object[] args)
        {
            logger.ErrorFormat(format, args);
        }
        public void Error(string message)
        {
            logger.Error(message);
        }
        public void ErrorException(string message, Exception exception)
        {
            logger.Error(message, exception);
        }
        public void Fatal(Exception exception, string format, params object[] args)
        {
            logger.Fatal(exception.Message, exception);
        }
        public void Fatal(string format, params object[] args)
        {
            logger.FatalFormat(format, args);
        }
        public void Fatal(string message)
        {
            logger.Fatal(message);
        }
        public void FatalException(string message, Exception exception)
        {
            logger.Fatal(message, exception);
        }
        public void Info(Exception exception, string format, params object[] args)
        {
            logger.Info(exception.Message, exception);
        }
        public void Info(string format, params object[] args)
        {
            logger.InfoFormat(format, args);
        }
        public void Info(string message)
        {
            logger.Info(message);
        }
        public void InfoException(string message, Exception exception)
        {
            logger.Info(message, exception);
        }
        public bool IsDebugEnabled
        {
            get { return logger.IsDebugEnabled; }
        }
        public bool IsErrorEnabled
        {
            get { return logger.IsErrorEnabled; }
        }
        public bool IsFatalEnabled
        {
            get { return logger.IsFatalEnabled; }
        }
        public bool IsInfoEnabled
        {
            get { return logger.IsInfoEnabled; }
        }
        public bool IsTraceEnabled
        {
            get { return false; }
        }
        public bool IsWarnEnabled
        {
            get { return logger.IsWarnEnabled; }
        }
        public string Name
        {
            get { return this.GetType().Name; }
        }
        public void Trace(Exception exception, string format, params object[] args)
        {
        }
        public void Trace(string format, params object[] args)
        {
        }
        public void Trace(string message)
        {
        }
        public void TraceException(string message, Exception exception)
        {
            throw new NotImplementedException();
        }
        public Type Type
        {
            get { return this.GetType(); }
        }
        public void Warn(Exception exception, string format, params object[] args)
        {
            throw new NotImplementedException();
        }
        public void Warn(string format, params object[] args)
        {
            logger.WarnFormat(format, args);
        }
        public void Warn(string message)
        {
            logger.Warn(message);
        }
        public void WarnException(string message, Exception exception)
        {
            logger.Warn(message, exception);
        }
    }
}
