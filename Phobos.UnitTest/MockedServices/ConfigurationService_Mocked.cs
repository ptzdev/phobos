﻿using Phobos.Library.Interfaces.Services;
using Phobos.Library.Models.ConfigSections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phobos.UnitTest.MockedServices
{
    public class ConfigurationService_Mocked : IConfigurationService
    {
        private static string _ApiKey;
        private static string _BaseUrl;
        private static string _DomainName;
        private static bool _TestMode = true;

        public string ApiKey
        {
            get { return _ApiKey; }
            set { _ApiKey = value; }
        }

        public string BaseUrl
        {
            get { return _BaseUrl; }
            set { _BaseUrl = value; }
        }

        public string DomainName
        {
            get { return _DomainName; }
            set { _DomainName = value; }
        }

        public bool TestMode
        {
            get { return _TestMode; }
            set { _TestMode = value; }
        }

        public ConfigurationService_Mocked()
        {
            _ApiKey = "key-ed6268300ba23c819a3482e348672f3f";
            _BaseUrl = "https://api.mailgun.net/v3";
            _DomainName = "sandbox11f65e9f553d42e3ba1d57a4266bb962.mailgun.org";
        }

        public ExternalMessagingConfigSection GetExternalMessagingConfigSection()
        {
            return new ExternalMessagingConfigSection()
            {
                ApiKey = this.ApiKey,
                BaseUrl = this.BaseUrl,
                Domain = new DomainElement() { Name = this.DomainName },
                From = new FromElement() { Name = "Phobos.UnitTest", Email = "noreply@phobos.pt" },
                TestMode = this.TestMode
            };
        }

        public CoreConfigSection GetCoreConfigSection()
        {
            return new CoreConfigSection()
            {
                UserManagement = new UserManagementElement()
                {
                    Minutes = 0,
                    Seconds = 1
                }
            };
        }
    }
}
