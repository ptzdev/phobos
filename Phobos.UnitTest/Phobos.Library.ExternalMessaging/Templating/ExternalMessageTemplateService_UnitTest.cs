﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Phobos.Library.ExternalMessaging;
using Phobos.Library.Interfaces.Services;
using Phobos.Library.Models;
using Phobos.Library.Models.Messaging;
using Phobos.UnitTest.MockedServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using Phobos.UnitTest;

namespace Phobos.UnitTest.Phobos.Library.ExternalMessaging.Templating
{
    [TestClass]
    public class ExternalMessageTemplateService_UnitTest
    {
        public IExternalMessageTemplateService externalMessageTemplateService { get; set; }

        [TestInitialize]
        public void Initialize()
        {
            var kernel = UnitTestUtils.GetMockedKernel();
            kernel.Rebind<IExternalMessageTemplateService>().To<ExternalMessageTemplateService>();
            externalMessageTemplateService = kernel.Get<IExternalMessageTemplateService>();
        }

        [TestMethod]
        [TestCategory("Phobos.Library.ExternalMessaging.ExternalMessageTemplate")]
        [TestProperty("UserAccount_Username", "dummyUser@phobos.pt")]
        public void GetNewProfileMessage()
        {
            var currenMethod = GetType().GetMethod(System.Reflection.MethodInfo.GetCurrentMethod().Name);

            UserAccount userAccount = UnitTestUtils.GetUserAccountBasedOnTestProperties(currenMethod);

            ExternalMessage msg = externalMessageTemplateService.GetNewProfileMessage(userAccount);

            Assert.IsTrue(msg.To.Any() || msg.Cc.Any() || msg.Bcc.Any(), "A message should have at least a recipient");
            Assert.IsFalse(string.IsNullOrEmpty(msg.Subject), "A message should have a subject");
            Assert.IsFalse(string.IsNullOrEmpty(msg.Body), "A message should have a body");
        }

        [TestMethod]
        [TestCategory("Phobos.Library.ExternalMessaging.ExternalMessageTemplate")]
        [TestProperty("UserAccount_Username", "dummyUser@phobos.pt")]
        public void GetRecoverProfileMessage()
        {
            var currenMethod = GetType().GetMethod(System.Reflection.MethodInfo.GetCurrentMethod().Name);

            UserAccount userAccount = UnitTestUtils.GetUserAccountBasedOnTestProperties(currenMethod);

            ExternalMessage msg = externalMessageTemplateService.GetRecoverProfileMessage(userAccount);

            Assert.IsTrue(msg.To.Any() || msg.Cc.Any() || msg.Bcc.Any(), "A message should have at least a recipient");
            Assert.IsFalse(string.IsNullOrEmpty(msg.Subject), "A message should have a subject");
            Assert.IsFalse(string.IsNullOrEmpty(msg.Body), "A message should have a body");
        }


        [TestMethod]
        [TestCategory("Phobos.Library.ExternalMessaging.ExternalMessageTemplate")]
        [TestProperty("UserAccount_Username", "dummyUser@phobos.pt")]
        public void GetLastLoginNotification()
        {
            var currenMethod = GetType().GetMethod(System.Reflection.MethodInfo.GetCurrentMethod().Name);

            UserAccount userAccount = UnitTestUtils.GetUserAccountBasedOnTestProperties(currenMethod);
            UserNotification msg = externalMessageTemplateService.GetLastLoginNotification(userAccount);

            Assert.IsTrue(msg.Type == global::Phobos.Library.Models.Enums.NotificationType.Login, "The type is incorrect");
            Assert.IsFalse(string.IsNullOrEmpty(msg.Title), "A message should have a Title");
        }

        [TestMethod]
        [TestCategory("Phobos.Library.ExternalMessaging.ExternalMessageTemplate")]
        [TestProperty("UserAccount_Username", "dummyUser@phobos.pt")]
        public void GetWelcomeNotification()
        {
            var currenMethod = GetType().GetMethod(System.Reflection.MethodInfo.GetCurrentMethod().Name);

            UserAccount userAccount = UnitTestUtils.GetUserAccountBasedOnTestProperties(currenMethod);
            UserNotification msg = externalMessageTemplateService.GetWelcomeNotification(userAccount);

            Assert.IsTrue(msg.Type == global::Phobos.Library.Models.Enums.NotificationType.Welcome, "The type is incorrect");
            Assert.IsFalse(string.IsNullOrEmpty(msg.Title), "A message should have a Title");
        }
    }
}
