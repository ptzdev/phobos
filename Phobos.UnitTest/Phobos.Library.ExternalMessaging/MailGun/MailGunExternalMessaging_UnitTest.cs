﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Phobos.Library.ExternalMessaging;
using Phobos.Library.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using Phobos.Library.Models;
using Phobos.Library.Models.Messaging;
using System.Reflection;
using Phobos.UnitTest.MockedServices;

namespace Phobos.UnitTest.Phobos.Library.ExternalMessaging.MailGun
{
    [TestClass]
    public class MailGunExternalMessaging_UnitTest
    {
        public IExternalMessageService mailGunExternalMessaging { get; set; }
        bool transformEmptyToNull { get; set; }
        ConfigurationService_Mocked configurationService_Mocked { get; set; }

        [TestInitialize]
        public void Initialize()
        {
            var kernel = UnitTestUtils.GetMockedKernel();
            kernel.Rebind<IExternalMessageService>().To<MailGunExternalMessaging>();

            configurationService_Mocked = (ConfigurationService_Mocked)kernel.Get<IConfigurationService>();
            mailGunExternalMessaging = kernel.Get<IExternalMessageService>();

            transformEmptyToNull = false;
        }

        [TestCleanup]
        public void CleanUp()
        {
            this.configurationService_Mocked.TestMode = true;
        }

        [TestMethod]
        [TestCategory("Phobos.Library.ExternalMessaging.MailGun")]
        [TestProperty("UserAccount_Username", "dummyUser@phobos.pt")]
        [TestProperty("ExternalMessage_To", "bla@nonExistingDomain.pt")]
        [TestProperty("ExternalMessage_Subject", "This is an email unitTest Email")]
        [TestProperty("ExternalMessage_Body", "This is an email unitTest Email, please ignore")]
        public void SendMessageToNonExistingEmail()
        {
            var currenMethod = GetType().GetMethod(System.Reflection.MethodInfo.GetCurrentMethod().Name);
            this.SendEmail(currenMethod);
        }

        [TestMethod]
        [TestCategory("Phobos.Library.ExternalMessaging.MailGun")]
        [TestProperty("UserAccount_Username", "dummyUser@phobos.pt")]
        [TestProperty("ExternalMessage_To", "pedro.torrezao@gmail.com")]
        [TestProperty("ExternalMessage_Subject", "This is an email unitTest Email")]
        [TestProperty("ExternalMessage_Body", "This is an email unitTest Email, please ignore")]
        [ExpectedException(typeof(ExternalMessagingException))]
        public void SendMessageToExistingEmailButWithWrongAPIKey()
        {
            var currenMethod = GetType().GetMethod(System.Reflection.MethodInfo.GetCurrentMethod().Name);
            var oldValue = configurationService_Mocked.ApiKey;
            configurationService_Mocked.ApiKey = "key-00000000000000000000000000000000";

            this.SendEmail(currenMethod);
        }

        [TestMethod]
        [TestCategory("Phobos.Library.ExternalMessaging.MailGun")]
        [TestProperty("UserAccount_Username", "dummyUser@phobos.pt")]
        [TestProperty("ExternalMessage_To", "pedro.torrezao@gmail.com")]
        [TestProperty("ExternalMessage_Subject", "This is an email unitTest Email")]
        [TestProperty("ExternalMessage_Body", "This is an email unitTest Email, please ignore")]
        public void SendMessageToExistingEmail()
        {
            var currenMethod = GetType().GetMethod(System.Reflection.MethodInfo.GetCurrentMethod().Name);
            this.SendEmail(currenMethod);
        }

        [TestMethod]
        [TestCategory("Phobos.Library.ExternalMessaging.MailGun")]
        [TestProperty("UserAccount_Username", "dummyUser@phobos.pt")]
        [TestProperty("ExternalMessage_To", "pedro.torrezao@gmail.com")]
        [TestProperty("ExternalMessage_Subject", "This is an email unitTest Email")]
        [TestProperty("ExternalMessage_Body", "This is an email unitTest Email, please ignore")]
        public void SendMessageToMultipleEmails()
        {
            var outputMsg = "";
            var currenMethod = GetType().GetMethod(System.Reflection.MethodInfo.GetCurrentMethod().Name);
            
            UserAccount userAccount = UnitTestUtils.GetUserAccountBasedOnTestProperties(currenMethod, this.transformEmptyToNull);
            ExternalMessage externalMessage = UnitTestUtils.GetExternalMessageBasedOnTestProperties(currenMethod, this.transformEmptyToNull);

            externalMessage.To.Add("to1@phobos.pt");
            externalMessage.To.Add("to2@phobos.pt");
            externalMessage.Cc.Add("cc1@phobos.pt");
            externalMessage.Cc.Add("cc2@phobos.pt");
            externalMessage.Cc.Add("cc3@phobos.pt");
            externalMessage.Bcc.Add("bcc1@phobos.pt");

            var result = mailGunExternalMessaging.SendMessage(userAccount, externalMessage, ref outputMsg);

            Assert.IsFalse(outputMsg == string.Empty, string.Format("The Service didn't get a correct outputmsg ({0})", outputMsg));
            Assert.IsTrue(result, string.Format("The Service wasn´t able to send the message because ({0})", outputMsg));
  
        }

        [TestMethod]
        [TestCategory("Phobos.Library.ExternalMessaging.MailGun")]
        [TestProperty("UserAccount_Username", "dummyUser@phobos.pt")]
        [TestProperty("ExternalMessage_To", "pedro.torrezao@gmail.com")]
        [TestProperty("ExternalMessage_Subject", "")]
        [TestProperty("ExternalMessage_Body", "")]
        public void SendMessageToExistingEmailWithNullContent()
        {
            transformEmptyToNull = true;
            var currenMethod = GetType().GetMethod(System.Reflection.MethodInfo.GetCurrentMethod().Name);
            var outputMsg = "";

            UserAccount userAccount = UnitTestUtils.GetUserAccountBasedOnTestProperties(currenMethod, this.transformEmptyToNull);
            ExternalMessage externalMessage = UnitTestUtils.GetExternalMessageBasedOnTestProperties(currenMethod, this.transformEmptyToNull);

            var result = mailGunExternalMessaging.SendMessage(userAccount, externalMessage, ref outputMsg);

            Assert.IsFalse(outputMsg == string.Empty, string.Format("The Service didn't get a correct outputmsg ({0})", outputMsg));
            Assert.IsFalse(result, string.Format("The Service sent a message but shouldn't be allowd ({0})", outputMsg));

            transformEmptyToNull = false;
        }

        [TestMethod]
        [TestCategory("Phobos.Library.ExternalMessaging.MailGun")]
        [TestProperty("UserAccount_Username", "dummyUser@phobos.pt")]
        [TestProperty("ExternalMessage_To", "to1@phobos.pt")]
        [TestProperty("ExternalMessage_Subject", "This is an email unitTest Email")]
        [TestProperty("ExternalMessage_Body", "This is an email unitTest Email, please ignore")]
        public void SendMessageToExistingEmailButNotAddedToTestList()
        {
            transformEmptyToNull = true;
            var currenMethod = GetType().GetMethod(System.Reflection.MethodInfo.GetCurrentMethod().Name);
            var outputMsg = "";

            UserAccount userAccount = UnitTestUtils.GetUserAccountBasedOnTestProperties(currenMethod, this.transformEmptyToNull);
            ExternalMessage externalMessage = UnitTestUtils.GetExternalMessageBasedOnTestProperties(currenMethod, this.transformEmptyToNull);

            var result = mailGunExternalMessaging.SendMessage(userAccount, externalMessage, ref outputMsg);

            Assert.IsFalse(outputMsg == string.Empty, string.Format("The Service didn't get a correct outputmsg ({0})", outputMsg));
            Assert.IsTrue(result, string.Format("The Service sent a message but shouldn't be allowd ({0})", outputMsg));

            transformEmptyToNull = false;
        }

        [TestMethod]
        [TestCategory("Phobos.Library.ExternalMessaging.MailGun")]
        [TestProperty("UserAccount_Username", "dummyUser@phobos.pt")]
        [TestProperty("ExternalMessage_To", "to1@phobos.pt")]
        [TestProperty("ExternalMessage_Subject", "This is an email unitTest Email")]
        [TestProperty("ExternalMessage_Body", "This is an email unitTest Email, please ignore")]
        [ExpectedException(typeof(ExternalMessagingException))]
        public void SendMessageToExistingEmailButNotAddedToTestListOnNotTest()
        {
            transformEmptyToNull = true;
            var currenMethod = GetType().GetMethod(System.Reflection.MethodInfo.GetCurrentMethod().Name);
            var outputMsg = "";

            UserAccount userAccount = UnitTestUtils.GetUserAccountBasedOnTestProperties(currenMethod, this.transformEmptyToNull);
            ExternalMessage externalMessage = UnitTestUtils.GetExternalMessageBasedOnTestProperties(currenMethod, this.transformEmptyToNull);

            this.configurationService_Mocked.TestMode = false;

            var result = mailGunExternalMessaging.SendMessage(userAccount, externalMessage, ref outputMsg);
        }


        private void SendEmail(MethodInfo currenMethod)
        {
            UserAccount userAccount = UnitTestUtils.GetUserAccountBasedOnTestProperties(currenMethod, this.transformEmptyToNull);
            ExternalMessage externalMessage = UnitTestUtils.GetExternalMessageBasedOnTestProperties(currenMethod, this.transformEmptyToNull);
            var outputMsg = "";

            var result = mailGunExternalMessaging.SendMessage(userAccount, externalMessage, ref outputMsg);

            Assert.IsFalse(outputMsg == string.Empty, string.Format("The Service didn't get a correct outputmsg ({0})", outputMsg));
            Assert.IsTrue(result, string.Format("The Service wasn´t able to send the message because ({0})", outputMsg));
        }
    }
}
