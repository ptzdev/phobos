﻿using Ninject;
using Phobos.UnitTest.MockedServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Extensions.Logging;
using Phobos.Library.Interfaces.Services;
using Phobos.Library.Models;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Phobos.Library.Models.Messaging;

namespace Phobos.UnitTest
{
    public static class UnitTestUtils
    {
        public static IKernel GetMockedKernel()
        {
            var kernel = MvcApplication.GetKernel();

            kernel.Rebind<ILogger>().To<Logger_Mocked>();
            kernel.Rebind<IConfigurationService>().To<ConfigurationService_Mocked>();

            return kernel;
        }

        internal static UserAccount GetUserAccountBasedOnTestProperties(MethodInfo currenMethod, bool transformEmptyToNull = false)
        {
            return UnitTestUtils.GetObjectBasedOnTestProperties<UserAccount>(null, currenMethod, "UserAccount", transformEmptyToNull);
        }

        internal static ExternalMessage GetExternalMessageBasedOnTestProperties(MethodInfo currenMethod, bool transformEmptyToNull = false)
        {
            return UnitTestUtils.GetObjectBasedOnTestProperties<ExternalMessage>(null, currenMethod, "ExternalMessage", transformEmptyToNull);
        }
        internal static UserNotification GetUserNotificationBasedOnTestProperties(MethodInfo currenMethod, bool transformEmptyToNull = false)
        {
            return UnitTestUtils.GetObjectBasedOnTestProperties<UserNotification>(null, currenMethod, "UserNotification", transformEmptyToNull);
        }

        private static T GetObjectBasedOnTestProperties<T>(this object obj, MethodInfo currenMethod, string propertyPrefix, bool transformEmptyToNull = false)
        {
            var newObject = (T)Activator.CreateInstance(typeof(T));
            object[] attributes = currenMethod.GetCustomAttributes(typeof(TestPropertyAttribute), false);
            PropertyInfo[] propertyInfos = newObject.GetType().GetProperties();

            Array.Sort(propertyInfos, delegate(PropertyInfo propertyInfo1, PropertyInfo propertyInfo2) { return propertyInfo1.Name.CompareTo(propertyInfo2.Name); });

            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                for (int i = 0; i < attributes.Length; i++)
                {
                    string name = ((TestPropertyAttribute)attributes[i]).Name;
                    string val = ((TestPropertyAttribute)attributes[i]).Value;

                    if (name.ToLower() == string.Format("{0}_{1}", propertyPrefix, propertyInfo.Name).ToLower())
                    {
                        if (propertyInfo.PropertyType == typeof(string) && transformEmptyToNull && string.IsNullOrEmpty(val))
                        {
                            val = null;
                        }

                        if (propertyInfo.PropertyType == typeof(List<string>))
                        {
                            List<string> list = (List<string>)propertyInfo.GetValue(newObject);
                            list = list ?? new List<string>();
                            list.Add(val);
                        }
                        else
                        {
                            propertyInfo.SetValue(newObject, val);
                        }
                    }


                }
            }

            return newObject;
        }


    }
}
